window.addEventListener('load', function() {
  document.querySelector('body').addEventListener('click', function(event) {
    var target = event.target;
    var modalSelector = target.dataset.modalSelector;

    if (target.classList.contains('js-open-modal')) {
      new TestModal(modalSelector).open();
    } else if (target.classList.contains('js-close-modal')) {
      new TestModal(modalSelector).close();
    } else if (target.classList.contains('js-toggle-modal')) {
      new TestModal(modalSelector).toggle();
    }
  });
});