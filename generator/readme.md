Generator function is a new type of functions. It has a specific syntax.
To declare a generator function you need to use `function*`.
Inside generator function we use a `yield` keyword which provide us a control of generator execution process.

When we run generator function, function is not executed. Instead of this it returns a generator object.
Generator object provide us a `next()` method which allows us to control execution of generator function.
Each `generator.next()` execute code until following `yield` in generator function and then return a new generator state.
Generator state is an object: `{ value: '...', done: true/false }`.
When generator function is finished `done` in returning generator state is `true`.

We also have an ability to pass a value into generator function. In example below we pass a username and generator function continue execution and based on this value return next generator state.


Example of generator function:

```
function* helloGenerator() {
    const username = yield "Hello, what is you name?";
    yield `nice to meet you ${username}!`
}
```

```
const testHelloGenerator = helloGenerator(); // generator function is not executed. it returns a generator object

testHelloGenerator.next() // first run of generator. All code before first yield is executing. State is: {value: "Hello, what is you name?", done: false}
testHelloGenerator.next(‘John Doe’) // pass a value into generator and get back it’s state { value: "nice to meet you John Doe", done: false }
testHelloGenerator.next() // there are no more yield keywords left in generator function so we get { value: undefined, done: true }
```
