# Test Promise implementation

### Implementation Notes:

To implement a Promise, there is a standard document to refer to. Really proper way to do it would be to refer to [the standard](http://www.ecma-international.org/ecma-262/6.0/#sec-promise-objects) and base your code and tests on that.
 
I didn't do it to save time since I believe my implementation covers 99% of Promise use-cases and really deep implementation is not a goal of this test task.

