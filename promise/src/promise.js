const STATE_PENDING = Symbol('pending');
const STATE_FULFILLED = Symbol('resolved');
const STATE_REJECTED = Symbol('rejected');


module.exports = class Promise {
  constructor(executor) {
    this.__state = new PromiseState();
    runPromiseExecutor(executor, this.__state);
  }

  then(onFulfilled, onRejected) {
    return new Promise((fulfillChild, rejectChild) => {
      this.__state.addResolveHandler(result => runPromiseResolution(result, onFulfilled, fulfillChild, rejectChild));
      this.__state.addRejectHandler(error => runPromiseResolution(error, onRejected, fulfillChild, rejectChild));
    });
  }

  finally(handler) {
    return this.then(handler, handler);
  }

  catch(onRejected) {
    return this.then(null, onRejected);
  }

  static resolve(value) {
    return new Promise(resolve => resolve(value))
  }

  static reject(error) {
    return new Promise((resolve, reject) => reject(error));
  }
};


class PromiseState {
  constructor() {
    this.state = STATE_PENDING;
    this.result = null;
    this.error = null;
    this.fulfillHandlers = [];
    this.rejectHandlers = [];
  }

  resolve(result) {
    if (this.state !== STATE_PENDING) {
      throw new Error('promise state is not pending');
    }

    this.result = result;
    this.state = STATE_FULFILLED;

    for (const handler of this.fulfillHandlers) {
      handler(result);
    }
  }

  reject(error) {
    if (this.state !== STATE_PENDING) {
      throw new Error('promise state is not pending');
    }

    this.error = error;
    this.state = STATE_REJECTED;

    for (const handler of this.rejectHandlers) {
      handler(error);
    }
  }

  addResolveHandler(handler) {
    if (!handler) {
      return;
    } else if (typeof handler !== 'function') {
      throw new Error('handler should be a function')
    }

    if (this.state === STATE_PENDING) {
      this.fulfillHandlers.push(handler);
    } else if (this.state === STATE_FULFILLED) {
      handler(this.result);
    }
  }

  addRejectHandler(handler) {
    if (!handler) {
      return;
    } else if (typeof handler !== 'function') {
      throw new Error('handler should be a function')
    }

    if (this.state === STATE_PENDING) {
      this.rejectHandlers.push(handler);
    } else if (this.state === STATE_REJECTED) {
      handler(this.error);
    }
  }
}

function runPromiseExecutor(executor, state) {
  const onFulfill = function(result) {
    if (state.state !== STATE_PENDING) {
      throw new Error('trying to fulfill a promise which is already fulfilled or rejected');
    }

    state.resolve(result);
  };

  const onReject = function(error) {
    if (state.state !== STATE_PENDING) {
      throw new Error('trying to fulfill a promise which is already fulfilled or rejected');
    }

    state.reject(error);
  };

  try {
    executor(onFulfill, onReject);
  } catch (e) {
    if (this.__state.state === STATE_PENDING) {
      onReject(e);
    }
  }
}

function runPromiseResolution(result, handler, onFulfill, onReject) {
  try {
    const childResult = handler(result);

    if (isThenable(childResult)) {
      childResult.then(onFulfill, onReject);

    } else {
      onFulfill(childResult);
    }

  } catch (e) {
    onReject(e);
  }
}

function isThenable(something) {
  return typeof something.then === 'function';
}