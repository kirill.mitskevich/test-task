const Promise = require('../src/promise');

const sinon = require('sinon');
const { expect } = require('chai');


describe('Promise', function() {
  it('module exports a constructor', function() {
    expect(Promise).to.be.a('function');
  });

  describe('then', function() {
    it('calls resolution handler when parent gets fulfilled', function(done) {
      const promise = new Promise(resolve => setTimeout(() => resolve('hello'), 0));

      const onFulfilled = sinon.spy();
      const onRejected = sinon.spy();

      promise
        .then(onFulfilled, onRejected)
        .finally(function() {
          try {
            expect(onFulfilled.calledOnce).to.be.true;
            expect(onFulfilled.calledWith('hello')).to.be.true;
            expect(onRejected.notCalled).to.be.true;
            done();
          } catch (e) {
            done(e);
          }
        });
    });

    it('calls rejection handler when parent gets rejected', function(done) {
      const promise = new Promise((resolve, reject) => setTimeout(() => reject(new Error('some error')), 0));

      const onFulfilled = sinon.spy();
      const onRejected = sinon.spy();

      promise
        .then(onFulfilled, onRejected)
        .finally(function() {
          try {
            expect(onFulfilled.notCalled).to.be.true;
            expect(onRejected.calledOnce).to.be.true;
            done();
          } catch (e) {
            done(e);
          }
        });
    });

    describe('resolution handler', function() {
      it ('is rejects following promise in a chain if throws an exception', function() {
        const onFulfilled = sinon.spy();
        const onRejected = sinon.spy();

        Promise.resolve('123')
          .then(() => { throw new Error('some error') })
          .then(onFulfilled, onRejected);

        expect(onFulfilled.notCalled).to.be.true;
        expect(onRejected.calledOnce).to.be.true;
      });

      it ('waits for another promise if resolution handler returns it', function() {
        const onFulfilled = sinon.spy();
        const onRejected = sinon.spy();

        Promise.resolve('123')
          .then(() => new Promise((resolve, reject) => resolve('345')))
          .then(onFulfilled, onRejected);

        expect(onFulfilled.calledWith('345')).to.be.true;
        expect(onRejected.notCalled).to.be.true;
      });
    });
  });
});